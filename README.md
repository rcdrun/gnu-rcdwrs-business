GNU RCD WRS for Business
------------------------

## Business Management and Website Revision System

GNU RCD WRS for Business is Business Management and Website Revision
System with email follow-up and autoresponder features. It will be
written in Guile, converted from the existing Perl. It uses PostgreSQL
and GNU GPL software.

It combines following features for the modern entrepreneur:

- Website Revision System ([WRS])
- Contact Management
- Email Follow-Up

Features:

* Accounts, which may be:
	* Companies
	* Groups
	* Mailing Lists
	* Bank Accounts
	* Cash Accounts

* Contacts, which may belong to accounts, such as:
	* Main Account
	* Company
	* Other Account

* Opportunities

* Notes, for:
	* Accounts
	* Contacts
	* Opportunities
	* Cases

* Calls

* Emails
	* Mailing Lists
	* Newsletter management
	* Offline autoresponder for letters

* Marks

* Reminders


## Website Revision System

* Pages
* Links
* Products
* Real Estate
* FAQ Categories, Questions and Answers
* Gold and Silver prices
* Fee Schedules and Fees
	* PDF Generation
	* HTML table generation within WRS

There are maybe 100 domains and websites generated and created by using
the GNU RCD WRS for Business.

## Business Management Features

* Invoices
* Good and Services
* Cash accounts and basic double entry accounting
* Cash journal
* Administrative Scales, Programs, Plans, Projects, Policies, Ideal Scenes, Valuable Final Products
* Statistics
* Real Estate
* Top Level Domain management and TEL Domain Management
* Real Estate
* Gold and Silver
* Fee Schedules and Fees

## Software required

* [GNU operating system], such as [Trisquel] or other [free operating system]
* [Guile]
* [PostgreSQL]
* [Gedafe] - Generic web based frontend for PostgreSQL
* [Perl] and many Perl modules

## Installation

Installation procedure is not yet finished.

1. It works on the [GNU operating system]
2. Install PostgreSQL
3. Install Guile
4. Install Perl
5. Create user in PostgreSQL:
	createuser gnurcdwrsadmin
	Shall the new role be a superuser? (y/n) n  
	Shall the new role be allowed to create databases? (y/n) y  
	Shall the new role be allowed to create more new roles? (y/n) y  

**MUCH TODO HERE**

## Credits

Credits to:

- [GNU] operating system and [free software philosophy]

- Many thanks to @oetiker for development of [Gedafe], the Generic Database
Frontend, together with his coleagues in Switzerland.

[WRS]: https://www.gnu.org/philosophy/words-to-avoid.en.html#Content "Website Revision System"
[Guile]: http://www.gnu.org/software/guile/ "Guile Programming Language"
[free software philosophy]: https://www.gnu.org/philosophy/ "Philosophy of the GNU Project"
[GNU operating system]: https://www.gnu.org "GNU Operating System"
[GNU]: https://www.gnu.org "GNU Operating System"
[Gedafe]: https://gedafe.github.io/index.en.html "Gedafe is Generic, Web-based, PostgreSQL Database Front-end"
[Trisquel]: https://trisquel.info/ "Trisquel GNU/Linux is a fully free operating system"
[free operating system]: http://www.gnu.org/distros/free-distros.html "Free GNU/Linux distributions"
[PostgreSQL]: http://www.postgresql.org "PostgreSQL"
[Perl]: https://www.perl.org/get.html "Perl"
